FROM alpine:edge AS download
RUN apk add --no-cache \
      curl ca-certificates tar gzip \
 && curl -o cubesql_linux64bit.tar.gz https://sqlabs.com/download/cubesql/latest/cubesql_linux64bit.tar.gz \
 && tar -xzf cubesql_linux64bit.tar.gz

FROM frolvlad/alpine-glibc
COPY --from=download /cubesql_64bit/data/cubesql /usr/local/bin/cubesql

ENV CUBESQL_GID="800" \
    CUBESQL_UID="800" \
    CUBESQL_USER="cubesql" \
    CUBESQL_GROUP="cubesql" \
    CUBESQL_DATA="/data" \
    CUBESQL_SETTINGS="/data/cubesql.settings"

RUN chmod +x /usr/local/bin/cubesql \
 && addgroup -S -g "${CUBESQL_GID}" cubesql \
 && adduser -S -D -H -G cubesql -u "${CUBESQL_UID}" cubesql \
 && mkdir -p "${CUBESQL_DATA}" \
 && chown -R cubesql:cubesql "${CUBESQL_DATA}"

EXPOSE 4430
VOLUME [ "${CUBESQL_DATA}" ]
USER cubesql

CMD /usr/local/bin/cubesql -f CONSOLE -p 4430 -x $CUBESQL_DATA -s $CUBESQL_SETTINGS
